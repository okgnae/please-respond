import json
import requests
import datetime

time_loop = datetime.datetime.now().timestamp() + 60
events = {}

# function to agrigate country_count_list to dict, sum counts per country
def per_country(country_count_list):
    per_country_dict = {}
    for i in range(len(country_count_list)):
        # if county not found create entry in dict
        if country_count_list[i][0] not in per_country_dict:
            per_country_dict.update({country_count_list[i][0] : country_count_list[i][1]})
        # if county in dict get current count and add new count, update dict with new {country : count}
        if country_count_list[i][0] in per_country_dict:
            current_val = per_country_dict[country_count_list[i][0]]
            new_val = current_val + country_count_list[i][1]
            per_country_dict.update({country_count_list[i][0] : new_val})
    return per_country_dict

# function to find countries with highest counts
def top_countries(country_count_list):
    # call per_country_dict and unpack as list of (key val) tuple
    per_country_list = [(country, count) for country, count in per_country(country_count_list).items()]
    # sort list based on element one (count) of each tuple
    per_country_list.sort(key = lambda elm: elm[1], reverse=True)
    return ','.join([f"{country},{count}" for country, count in per_country_list[:3]])

# function to return top 3 latests event based on time from events dict
def latest_event(events):
    # unpack events keys as list of (time, url) tupels
    event_time_url = [(time, url) for time, url in events.keys()]
    # sort list based on element 0 (time) of each tuple
    event_time_url.sort(key = lambda elm: elm[0], reverse=True)
    return ','.join((event_time_url[0][0], event_time_url[0][1]))

# Create requests session
with requests.Session() as s:
    # Connect as stream to url
    stream = s.get('http://stream.meetup.com/2/rsvps', stream=True)
    # read steam per line
    for line in stream.iter_lines():
        # if line is blank skip
        if line:
            # decode to json
            json_line = json.loads(line.decode('utf-8'))

            # exstract values from json
            event_url = json_line['event']['event_url']
            event_time = datetime.datetime.fromtimestamp(json_line['event']['time']/1000).strftime('%Y-%m-%d %H:%M')
            event_country = json_line['group']['group_country']
            guest_count = json_line['guests']

            # start building events dict with (event_time, event_url) tuple and key
            if events.get((event_time, event_url)) == None:
                events.update({(event_time, event_url) : {event_country : [guest_count]}})

            # if (event_time, event_url) exists but new county has guests update guest_count list
            if events.get((event_time, event_url)).get(event_country) == None:
                events.get((event_time, event_url)).update({event_country : [guest_count]})
            
            # update existing dict records
            else:
                events.get((event_time, event_url)).get(event_country).append(guest_count)

            # check time_loop if 60 seconds has passed since last loop
            if time_loop <= datetime.datetime.now().timestamp():
                # up date time_loop var with now() + 60 seconds
                time_loop = datetime.datetime.now().timestamp() + 60
                country_count_list = []
                
                # create country_count_list of tupels for all countries/guests over last time_loop, [(country,count),(country,count)] 
                for event in events:
                    for country in events[event]:
                        country_count_list.append( (country, sum(events[event][country])) )

                total_guests = sum(per_country(country_count_list)[i] for i in per_country(country_count_list))
                top_three = top_countries(country_count_list)
                event_time_url = latest_event(events)

                # print to stdout
                f"{total_guests},{event_time_url},{top_three}"
                # clear events dict
                events = {}
